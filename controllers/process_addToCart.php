<?php 
	//
	session_start();

	$name = $_POST["name"];
	$quantity = $_POST["quantity"];
	//isset() checks if the item is existing
	if(!isset($_SESSION["cart"][$name])){
		$_SESSION["cart"][$name] = $quantity;
	} else {
		$_SESSION["cart"][$name] += $quantity;
	};

	

	//to check if the input quantity is being processed. use var_dump and die()
	// var_dump($quantity);
	// die();

	header("LOCATION: ../views/catalog.php");

?>