<?php 
	// use $_GET if sending data via URL/<a> tag
	$name = $_GET["name"];
	$products = file_get_contents("../assets/lib/products.json");
	$products_array = json_decode($products, true);

	//assign 
	foreach($products_array as $index => $product){
		if($name == $product["name"]){
			unset($products_array[$index]);
		};
	};

	$to_write = fopen("../assets/lib/products.json", "w");
	fwrite($to_write, json_encode($products_array, JSON_PRETTY_PRINT));
	fclose($to_write);

	header("LOCATION: ". $_SERVER["HTTP_REFERER"]);

?>

<!-- my usual issue is on the PATH LINKS -->